﻿using Platformer.Core;
using Platformer.Mechanics;
using Platformer.Model;

namespace Platformer.Gameplay
{
    /// <summary>
    /// Fired when a player enters a trigger with a DeathZone component.
    /// </summary>
    /// <typeparam name="PlayerEnteredCheckpoint"></typeparam>
    public class PlayerEnteredCheckpoint : Simulation.Event<PlayerEnteredCheckpoint>
    {
        public Checkpoint checkpoint;

        PlatformerModel model = Simulation.GetModel<PlatformerModel>();

        public override void Execute()
        {
            model.spawnPoint = checkpoint.spawnPoint;
        }
    }
}