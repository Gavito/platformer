﻿using Platformer.Core;
using Platformer.Mechanics;
using Platformer.Model;
using UnityEngine;

namespace Platformer.Gameplay
{
    /// <summary>
    /// Fired when a player collides with a power up.
    /// </summary>
    /// <typeparam name="PlayerPowerUpCollision"></typeparam>
    public class PlayerPowerUpCollision : Simulation.Event<PlayerPowerUpCollision>
    {
        public PlayerController player;
        public PowerUp powerUp;

        PlatformerModel model = Simulation.GetModel<PlatformerModel>();

        public override void Execute()
        {
            AudioSource.PlayClipAtPoint(powerUp.collectAudio, powerUp.transform.position);
            powerUp.StartCoroutine("StartPowerUp", player);
            powerUp.gameObject.SetActive(false);
        }
    }
}