﻿using System.Collections;
using UnityEngine;

namespace Platformer.Mechanics
{
    public class JumpPowerUp : PowerUp
    {
        /// <summary>
        /// Controls the jumpTakeOff given to the player;
        /// </summary>
        public float jumpTakeOff;
        /// <summary>
        /// Controls the gravity modifier of the kinematic component of the player;
        /// </summary>
        public float gravityModifier;
        /// <summary>
        /// The duration of the effect;
        /// </summary>
        public float duration;

        public override IEnumerator StartPowerUp(PlayerController player)
        {
            player.jumpTakeOffSpeed += jumpTakeOff;
            player.gravityModifier += gravityModifier;

            yield return new WaitForSeconds(duration);

            player.jumpTakeOffSpeed -= jumpTakeOff;
            player.gravityModifier -= gravityModifier;
        }
    }
}