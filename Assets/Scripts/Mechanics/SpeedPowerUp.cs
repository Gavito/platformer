﻿using System.Collections;
using UnityEngine;

namespace Platformer.Mechanics
{
    public class SpeedPowerUp : PowerUp
    {
        /// <summary>
        /// Controls the bonus speed given to the player;
        /// </summary>
        public float bonusSpeed;
        /// <summary>
        /// The duration of the effect;
        /// </summary>
        public float duration;

        public override IEnumerator StartPowerUp(PlayerController player)
        {
            player.maxSpeed += bonusSpeed;
            player.trail.SetActive(true);

            yield return new WaitForSeconds(duration);

            player.maxSpeed -= bonusSpeed;
            player.trail.SetActive(false);
        }
    }
}