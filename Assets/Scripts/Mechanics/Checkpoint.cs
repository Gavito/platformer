﻿using UnityEngine;
using System.Collections;
using Platformer.Gameplay;
using static Platformer.Core.Simulation;

namespace Platformer.Mechanics
{
    /// <summary>
    /// Checkpoint components mark a collider which will schedule a
    /// PlayerEnteredCheckpoint event when the player enters the trigger.
    /// </summary>
    public class Checkpoint : MonoBehaviour
    {
        public Transform spawnPoint;

        void OnTriggerEnter2D(Collider2D collider)
        {
            var p = collider.gameObject.GetComponent<PlayerController>();
            if (p != null)
            {
                var ev = Schedule<PlayerEnteredCheckpoint>();
                ev.checkpoint = this;
            }
        }
    }
}
