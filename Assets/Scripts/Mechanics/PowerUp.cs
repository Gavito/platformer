﻿using Platformer.Gameplay;
using UnityEngine;
using System.Collections;
using static Platformer.Core.Simulation;

namespace Platformer.Mechanics
{
    /// <summary>
    /// This class contains the data required for implementing power ups to the player.
    /// </summary>
    [RequireComponent(typeof(Collider2D))]
    public class PowerUp : MonoBehaviour
    {
        public AudioClip collectAudio;

        /// <summary>
        /// Controls the movement of the power up element
        /// </summary>
        private float speed = 5f, range = .1f;

        private Vector3 _startPosition;

        private void Start()
        {
            _startPosition = transform.position;
        }

        private void Update()
        {
            transform.position = _startPosition + new Vector3(0, Mathf.Sin(Time.time * speed) * range, 0);
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            //only exectue OnPlayerEnter if the player collides with this token.
            var player = other.gameObject.GetComponent<PlayerController>();
            if (player != null) OnPlayerEnter(player);
        }

        void OnPlayerEnter(PlayerController player)
        {
            //send an event into the gameplay system to perform some behaviour.
            var ev = Schedule<PlayerPowerUpCollision>();
            ev.powerUp = this;
            ev.player = player;
        }

        public virtual IEnumerator StartPowerUp(PlayerController player)
        {
            return null;
        }
    }
}